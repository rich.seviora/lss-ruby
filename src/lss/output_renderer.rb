module LSS
  # OutputRenderer is responsible for rendering the data to console.
  class OutputRenderer

    attr_reader :output_stream

    # @param [:width, :value] render_mode - Whether to render the value width or not.
    def initialize(output_stream = STDOUT, render_mode = :width)
      @output_stream = output_stream
      @render_mode   = render_mode
    end

    # @param [Array<LSS::FileGroup>] groups The array of FileGroups to render.
    def render_groups(groups)
      rendered_groups = groups.map {|group| render_group(group)}
      sorted_groups   = rendered_groups.sort {|a, b| a[1] <=> b[1]}
      sorted_groups.each {|x| output_stream.puts(x.join(' '))}
    end

    # Renders a group to STDOUT
    # @param [LSS::FileGroup] group
    # @param [Integer] filename_length
    # @param [Integer] count_length
    def render_group(group, filename_length = 50, count_length = 6)
      count_text    = group.length.to_s.ljust(count_length)
      filename_text = render_group_signature(group.group_signature).ljust(filename_length)
      ranges_text   = render_group_ranges(group.ranges)
      [count_text, filename_text, ranges_text]
    end

    # Converts the group signature to its display string.
    # @param [Array] group_signature
    # @return [String]
    def render_group_signature(group_signature)
      group_signature.map {|x| x.is_a?(Integer) ? render_variable_value(x) : x}.join('')
    end

    # Converts the group ranges (array of value pairs) to text.
    # @param [Array<Array<Integer>>] group_ranges
    # @return [String]
    def render_group_ranges(group_ranges)
      group_ranges.map {|x| x[0] == x[1] ? x[0] : x.join('-')}.join(' ')
    end

    # Outputs the the variable value.
    def render_variable_value(value)
      return '%d' if @render_mode == :value
      value > 1 ? "%0#{value}d" : '%d'
    end
  end
end
