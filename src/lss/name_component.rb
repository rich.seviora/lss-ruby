module LSS
  # The NameComponent type represents a block of the filename and its type.
  # They are either variable (represents a number), or fixed (represents an non-number string)
  class NameComponent

    # @type [:variable, :fixed] Returns the type of the component.
    attr_reader :type
    # @type [String] Returns the content string.
    attr_reader :contents

    # @param [String] contents - The contents of this component.
    def initialize(contents)
      @contents = contents
      @type = contents.match(/\d+/) ? :variable : :fixed
    end

    # Returns the signature value for this component. Returns the string for fixed components,
    # and 1 for variable components.
    # @param [:value] actual Whether to return the true value, or 1 for basic signature purposes.
    def signature_value(actual = false)
      return contents if type == :fixed
      actual ? contents.to_i : 1
    end

    # Returns the width signature value for this component. Returns the string for fixed components,
    # and the length for variable components.
    def width_signature_value
      type == :fixed ? contents : contents.length
    end
  end
end
