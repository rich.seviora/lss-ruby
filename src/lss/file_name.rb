module LSS
  # The FileName type represents a filename entry.
  #
  # To use, call <tt>.parse_filename</tt> with the filename to create the
  # object from.
  #
  # Filenames can be part of the same block so long as their signatures match, meaning:
  #
  # * They have the same number of components.
  # * The fixed components are in the same location, and are identical.
  # * The variable components are in the same location.
  class FileName

    # Parses the filename and returns a new FileName object.
    # @param [String] filename
    # @param [:width, :value] group_mode - The type of grouping employed.
    def self.from_filename(filename, group_mode = :width)
      # Split by changes between numeric and non-numeric.
      # Create a NameComponent from each block.
      # Create a new FileName object.
      blocks     = filename.split(/(\d+|\D+)/).reject { |x| x == '' }
      components = blocks.map { |x| NameComponent.new(x) }
      new(filename, components, group_mode)
    end

    # @return [String] The filename.
    attr_reader :filename
    # @return [Array<LSS::NameComponent>] The array of name components.
    attr_reader :components
    # @return [:width, :value]
    attr_reader :group_mode

    # @param [String] filename
    # @param [Array<LSS::NameComponent>] components
    # @param [:width, :value] group_mode - Indicates whether this filename
    # should be grouped on width or just the value.
    def initialize(filename, components, group_mode = :width)
      @filename   = filename.freeze
      @components = components.freeze
      @group_mode = group_mode
    end

    # Indexer access to the component array.
    # @param [Integer] value - Index of the component.
    def [](value)
      components[value]
    end

    # Generates and returns the signature array for this filename.
    # @return [Array<String,Integer>] Returns the signature array.
    def signature
      @signature ||= if group_mode == :value
                       components.map(&:signature_value).freeze
                     else
                       components.map(&:width_signature_value).freeze
                     end
    end

    def output_signature
      @output_signature ||= components.map(&:contents).freeze
    end

    # Generates and returns the sort signature for this filename.
    # @return [Array<String,Integer>] Returns the sort signature array.
    def component_values
      @sort_signature ||= components.map { |x| x.signature_value(true) }.freeze
    end

    # Returns true if this matches the provided FileName or signature array.
    # @param [LSS::FileName, Array<String,Integer>] filename_or_array Filename to test for matching or signature array.
    # @return [TrueClass, FalseClass]
    def has_same_signature?(filename_or_array)
      signature_array = if filename_or_array.is_a?(LSS::FileName)
                          filename_or_array.signature
                        else
                          filename_or_array
                        end
      signature == signature_array
    end

    # Given another filename, determine how many components have changed.
    # @param [LSS::FileName] other_filename
    # @return [Integer] Number of changed components.
    def change_arity(other_filename)
      total_changes = 0
      max_length = [other_filename.component_values.length, component_values.length].max
      (0..(max_length - 1)).each do |index|
        total_changes += 1 unless other_filename.component_values[index] == component_values[index]
      end
      total_changes
    end

    # Compares this filename with another and returns the index of the first change.
    # @param [LSS::FileName] other_filename The other filename to check.
    def change_index(other_filename)
      max_length = [other_filename.component_values.length, component_values.length].max
      (0..(max_length - 1)).find_index do |index|
        other_filename.component_values[index] != component_values[index]
      end
    end
  end
end
