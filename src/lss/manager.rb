module LSS
  # The Manager class is responsible for coordinating the data processing between components.
  class Manager
    def self.process(path = Dir.pwd, render_mode = :width)
      raise ArgumentError, "Invalid Path: '#{path}'" unless Dir.exist?(path)
      names      = DirectoryReader.read_directory(path)
      file_names = names.map { |x| FileName.from_filename x, render_mode }
      grouper    = BlockGrouper.new(file_names)
      grouper.process_groups
      renderer = OutputRenderer.new(STDOUT, render_mode)
      renderer.render_groups grouper.resolved_groups
    end
  end
end
