module LSS
  # The DirectoryReader class is responsible for getting a list of filenames to process.
  class DirectoryReader
    # Reads a directory and returns an array of filenames (excluding paths)
    # @param [String] path The directory to read. Optional. Uses current working directory as default.
    # @return [Array<String>] An array of filename strings.
    def self.read_directory(path = Dir.pwd)
      absolute_filenames = Dir.entries(path).
          map { |fn| File.absolute_path(fn, path) }
      absolute_filenames.select { |x| File.file? x }.map { |abs_path| File.basename(abs_path) }
    end
  end
end
