module LSS
  # A FileGroup represents the collection of FileName objects that share a
  # common signature with (up to) one variable component.
  #
  # A FileGroup has #change_index, which is the index that the filenames in
  # the group can vary on.
  class FileGroup

    # @return [LSS::FileName]
    attr_reader :filenames
    # @return [Array<String,Number>]
    attr_reader :signature
    attr_reader :ranges

    # Creates a new file group from one filename.
    def self.from_filename(filename)
      new(filename.signature, [filename])
    end

    # @param [Array<LSS::FileName>] filenames A collection of associated filenames.
    # @param [Array<String,Number>] signature The filename signature.
    def initialize(signature, filenames)
      @filenames = filenames || []
      @signature = signature
      @ranges    = []
      @change_index = nil
    end

    def length
      filenames.length
    end

    alias size length

    # Returns the consolidated group signature, with an empty array for its variable values (if available).
    def group_signature
      first_name = filenames.first
      if filenames.length == 1
        first_name.output_signature
      elsif filenames.length >= 2
        result = first_name.output_signature.dup
        result[change_index] = first_name.signature[change_index]
        result
      else
        raise StandardError, 'Group Signature called with no contents'
      end
    end

    # Returns if the argument FileName can be added to this group.
    # If there are no filenames, than any filename with a matching signature
    # will be accepted.
    # If there is only one current filename, then any filename with a matching
    # signature and only one change is compatible.
    # If more than one, a change_index will have been assigned and only
    # filenames with a matching signature and a change at the existing change
    # index will be accepted.
    # @param [LSS::FileName] filename
    # @return [Boolean] True if compatible, false otherwise.
    def compatible?(filename)
      # If No Change Index, Check
      if filenames.empty?
        filename.has_same_signature?(signature)
      elsif change_index.nil?
        existing_name = filenames.first
        existing_name.has_same_signature?(filename) &&
            (existing_name.change_arity(filename) == 1)
      else
        existing_name = filenames.first
        existing_name.change_arity(filename) == 1 && existing_name.change_index(filename) == change_index
      end
    end

    # Adds a filename object if permissible. Otherwise, returns false.
    # @return [False, Array<LSS::FileName>] Returns false if the filename isn't compatible, or the filenames array otherwise.
    def add(filename)
      return false unless compatible?(filename)
      if change_index.nil? && !filenames.empty?
        first_name    = filenames.first
        self.change_index = first_name.change_index(filename)
        if self.change_index.nil?
          raise StandardError, "Change Index Not Assigned"
        end
      end
      filenames.push filename
    end

    # Evaluates all the existing filenames and returns the value ranges as an array of value pairs.
    # [1,5] indicates that there is a range of filenames with the changing value ranging from 1 to 5 inclusive.
    # [3,3] indicates that there is one filename
    # Calculates and returns the ranges of values as an array of pairs of values
    # (stored as arrays [low, high]).
    # If the range is one value long, both values will be identical.
    # If there are no ranges returns an empty array.
    # @return [Array<Array<Number>>] Returns an array of arrays containing low-high pairs of ranges.
    def ranges
      return [] unless change_index
      # Get the changing values.
      sorted_values = filenames.map { |s| s.component_values[change_index] }.sort
      ranges        = []
      current_range = nil
      sorted_values.each do |value|
        if current_range.nil?
          new_range = [value, value]
          ranges << new_range
          current_range = new_range
        elsif current_range[1] + 1 == value
          current_range[1] = value
        else
          new_range     = [value, value]
          current_range = new_range
          ranges << new_range
        end
      end
      ranges
    end



    def change_index=(value)
      @change_index = value
    end

    def change_index
      @change_index
    end
  end
end
