module LSS
  # The BlockGrouper is responsible transforming a list of filenames into signature groups.
  class BlockGrouper

    # @return [Hash<Array,Array<LSS::FileName>]
    attr_reader :initial_groups
    # @return [Array<LSS::FileGroup>]
    attr_reader :resolved_groups

    # @param [Array<LSS::FileName>] file_name_array Array of filename objects.
    def initialize(file_name_array)
      @initial_groups  = file_name_array.group_by(&:signature)
      @resolved_groups = []
    end

    def process_groups
      initial_groups.each do |signature, filenames|
        arity = signature_arity(signature)
        if arity <= 1 || filenames.length == 1
          handle_simple_group(signature, filenames)
        else
          handle_complex_group(filenames)
        end
      end
      # Find the groups that have multiple variables, and process each group to
      # see which values are consistent and which aren't.
    end

    private

    def handle_simple_group(signature, filenames)
      new_group = FileGroup.from_filename(filenames.first)
      filenames[1..-1].each { |fn| new_group.add(fn) }
      resolved_groups.push new_group
      # Create new group, determine variable changing.
    end

    # Handles a complex group (where there are multiple variable components) and returns the processed group(s).
    # @param [Array<LSS::FileName>] filenames The collection of filenames to process.
    def handle_complex_group(filenames)
      # Sort the filenames by their sort signature.
      sorted_names  = filenames.sort_by(&:component_values)
      current_group = nil
      sorted_names.each_with_index do |filename, index|
        unless current_group
          current_group = create_new_group(filename)
          next
        end
        # Create a new group unless the existing group adds the new filename.
        unless current_group.add(filename)
          current_group = create_new_group(filename)
        end
      end
    end

    def create_new_group(filename)
      new_group = FileGroup.from_filename(filename)
      @resolved_groups.push new_group
      new_group
    end

    # @param [Array<String,Number>] signature The signature to process.
    # @return [Integer] The number of variable components.
    def signature_arity(signature)
      signature.count { |x| x.is_a?(Integer) }
    end

  end
end
