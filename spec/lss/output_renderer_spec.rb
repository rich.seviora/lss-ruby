require 'rspec'

RSpec.describe LSS::OutputRenderer do
  let(:stream_mock) { instance_double IO }
  let(:signature) { ['12', 'bob', 2, '.txt'] }
  let(:ranges) { [[1, 3], [4, 4], [5, 10]] }
  let(:length) { 5 }
  let(:group_mock) do
    instance_double LSS::FileGroup,
                    length:          5,
                    group_signature: signature,
                    ranges:          ranges
  end
  subject { described_class.new(stream_mock) }
  describe '#render_group' do
    it 'renders the text correctly' do
      count_text     = length.to_s.ljust(6)
      filename_text  = '12bob%02d.txt'.ljust(50)
      ranges_text    = '1-3 4 5-10'
      expected_array = [count_text, filename_text, ranges_text]
      expect(subject.render_group group_mock).to eql(expected_array)
    end
  end
  describe '#render_groups' do
    it 'ouputs the text correctly' do
      count_text    = length.to_s.ljust(6)
      filename_text = '12bob%02d.txt'.ljust(50)
      ranges_text = '1-3 4 5-10'
      expected_text = [count_text, filename_text, ranges_text].join(' ')
      expect(stream_mock).to receive(:puts).with(expected_text)
      subject.render_groups [group_mock]
    end
  end
end