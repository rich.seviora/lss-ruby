require 'rspec'

RSpec.describe LSS::FileGroup do

  # @return [LSS::FileName]
  def new_name(filename)
    LSS::FileName.from_filename(filename)
  end

  let(:first_filename) { new_name '12bob123.txt' }
  let(:second_filename) { new_name '12bob124.txt' }
  let(:third_filename) { new_name '12bob125.txt' }
  let(:fourth_name) { new_name '24bob124.txt' }
  let(:fifth_name) { new_name '24bob127.txt' }
  let(:incompatible_name) { new_name '12bob123.tx' }

  describe '#add' do
    context 'contains one filename' do
      subject { described_class.from_filename(first_filename) }
      it 'returns true when adding a compatible filename' do
        expect(subject.add(second_filename)).to be_truthy
      end
      it 'assigns the change_index' do
        subject.add(second_filename)
        is_expected.to have_attributes change_index: 2
      end
    end
  end

  describe '#group_signature' do
    subject { described_class.from_filename(first_filename) }
    it 'returns a complete signature for a single value' do
      result = subject.group_signature
      expect(result).to eql(['12', 'bob', '123', '.txt'])
    end
    it 'returns the width value in the group signature for multiple values' do
      subject.add(second_filename)
      subject.add(third_filename)
      result = subject.group_signature
      expect(result).to eql(['12', 'bob', 3, '.txt'])
    end
  end

  describe '#ranges' do
    subject { described_class.from_filename(first_filename) }
    before do
      subject.add(second_filename)
      subject.add(third_filename)
    end
    it 'returns the correct ranges for a simple range' do
      result = subject.ranges
      expect(result).to include([123, 125])
    end
    it 'returns the correct ranges for complex ranges' do
      %w(12bob127.txt 12bob128.txt 12bob130.txt 12bob132.txt 12bob133.txt).
          map { |x| new_name(x) }.each { |fn| subject.add(fn) }
      result = subject.ranges
      expect(result).to include([127, 128], [130, 130], [132, 133])
    end
  end

  describe '#compatible' do
    context 'contains one filename' do
      subject { described_class.from_filename(first_filename) }
      it 'returns false for the initial filename' do
        # False because it has no changes with the initial filename.
        # It would return false on a duplicate filename, but I'm assuming this
        # won't happen at this stage.
        is_expected.not_to be_compatible(first_filename)
      end
      it 'returns true for the two other compatible names' do
        is_expected.to be_compatible(second_filename)
        is_expected.to be_compatible(third_filename)
      end
      it 'returns false for the two incompatible names' do
        is_expected.not_to be_compatible(fourth_name)
        is_expected.not_to be_compatible(fifth_name)
      end
    end
    context 'contains no filenames' do
      subject { described_class.new([2, 'bob', 3, '.txt'], nil) }
      it 'returns true for any matching filename' do
        [first_filename, second_filename, third_filename, fourth_name, fifth_name].each do |name|
          is_expected.to be_compatible(name)
        end
      end
      it 'returns false for an incompatible filename' do
        is_expected.not_to be_compatible(incompatible_name)
      end
    end
    context 'contains two filenames' do
      subject { described_class.from_filename(first_filename) }
      before { subject.add(second_filename) }
      it 'returns true for the other name in the sequence' do
        is_expected.to be_compatible(third_filename)
      end
    end
  end
end