require 'rspec'

RSpec.describe LSS::NameComponent do
  describe '#initialize' do
    it 'assigns :fixed for non-numeric strings' do
      result = described_class.new('blah')
      expect(result).to have_attributes type: :fixed
    end
    it 'assigns :variable for numeric strings' do
      result = described_class.new('123')
      expect(result).to have_attributes type: :variable
    end
  end
  describe '#signature_value' do
    it 'returns the string for non-numeric strings' do
      result = described_class.new('blah')
      expect(result).to have_attributes signature_value: 'blah'
    end
    it 'returns 1 for non-numeric strings' do
      result = described_class.new('0000')
      expect(result).to have_attributes signature_value: 1
    end
    it 'returns the integer value if passed 1 for non-numeric strings' do
      result = described_class.new('0040')
      expect(result.signature_value(true)).to eq 40
    end
  end
end