require 'rspec'

RSpec.describe LSS::FileName do
  describe '.parse_filename' do
    subject { described_class }
    before do
      allow(LSS::NameComponent).to receive(:new).and_return(1, 2, 3, 4, 5)
    end
    it 'calls NameComponent with the correct parts' do
      subject.from_filename('blah123.blah')
      expect(LSS::NameComponent).to have_received(:new).with('blah')
      expect(LSS::NameComponent).to have_received(:new).with('123')
      expect(LSS::NameComponent).to have_received(:new).with('.blah')
    end
    it 'assigns the component attribute correctly' do
      result = subject.from_filename('blah123.blah')
      expect(result).to have_attributes components: [1, 2, 3]
    end
    it 'returns a FileName object' do
      expect(subject.from_filename('blah123.blah')).to be_a LSS::FileName
    end
    it 'assigns the filename' do
      result = subject.from_filename('blah123.blah')
      expect(result).to have_attributes filename: 'blah123.blah'
    end
  end

  describe '#signature' do
    let(:comp1) { instance_double LSS::NameComponent, signature_value: 1, width_signature_value: 2 }
    let(:comp2) { instance_double LSS::NameComponent, signature_value: 'bob',
    width_signature_value: 'bob'}
    let(:comp3) { instance_double LSS::NameComponent, signature_value: 1, width_signature_value: 3 }
    subject { described_class.new('01bob003', [comp1, comp2, comp3], mode) }
    context 'value mode' do
      let(:mode) { :value }
      it 'calls #signature_value for each of the components' do
        subject.signature
        [comp1, comp2, comp3].each do |comp|
          expect(comp).to have_received(:signature_value)
        end
      end
      it 'returns the correct signature array' do
        expect(subject.signature).to eql([1, 'bob', 1])
      end
    end
    context 'width mode' do
      let (:mode) { :width }
      it 'calls #signature_value for each of the components' do
        subject.signature
        [comp1, comp2, comp3].each do |comp|
          expect(comp).to have_received(:width_signature_value)
        end
      end
      it 'returns the correct signature array' do
        expect(subject.signature).to eql([2, 'bob', 3])
      end
    end
  end

  describe '#has_same_signature?' do
    context 'value mode' do
      valid_matches = [
          %w(bob123.txt bob124.txt),
          %w(bob123.txt bob124.txt),
          %w(file01_0040.rgb file01_0041.rgb),
          %w(file1.01.rgb file2.01.rgb)
      ]
      valid_matches.each do |match_group|
        it("Matches '#{match_group[0]}' to '#{match_group[1]}'") do
          first_name  = described_class.from_filename(match_group[0])
          second_name = described_class.from_filename(match_group[1])
          expect(first_name).to have_same_signature(second_name)
        end
      end

      invalid_matches = [
          %w(bob123.txt bob125.tx),
          %w(bob123.txt bob.txt),
          %w(123.txt 0.txt),
          %w(bob000123.txt bob124.txt),
          %w(123bob.txt, bob.txt),
          %w(file1.01.rgb file201.rgb)
      ]
      invalid_matches.each do |match_group|
        it("Does not match '#{match_group[0]}' to '#{match_group[1]}'") do
          first_name  = described_class.from_filename(match_group[0])
          second_name = described_class.from_filename(match_group[1])
          expect(first_name).not_to have_same_signature(second_name)
        end
      end
    end


  end

  describe '#change_arity' do
    values = [
        ['bob123.txt', 'bob124.txt', 1],
        ['123bob123.txt', '123bob124.txt', 1],
        ['bob123.txt', 'bob124.txt', 1],
        ['124bob124.txt', '125bob125.txt', 2]
    ]
    values.each do |value|
      first_name  = value[0]
      second_name = value[1]
      expected    = value[2]
      it "returns #{expected} for #{first_name} and #{second_name}" do
        first  = LSS::FileName.from_filename(first_name)
        second = LSS::FileName.from_filename(second_name)
        expect(first.change_arity(second)).to eq expected
      end
    end
  end
end