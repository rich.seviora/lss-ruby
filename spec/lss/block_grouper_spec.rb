require 'rspec'

RSpec.describe LSS::BlockGrouper do
  let(:comp1) do
    instance_double LSS::FileName,
                    signature:        [1, 'bob', 1],
                    component_values: [1, 'bob', 2]
  end
  let(:comp2) do
    instance_double LSS::FileName,
                    signature:        [1, 'bob', 1],
                    component_values: [1, 'bob', 1]
  end
  let(:comp3) do
    instance_double LSS::FileName, signature: [1, 'bob'], component_values: [1, 'bob', 1]
  end
  describe '.new' do
    it 'assigns values to correct groups' do
      result = described_class.new([comp1, comp2, comp3])
      expect(result.initial_groups[[1, 'bob']]).to eql([comp3])
      expect(result.initial_groups[[1, 'bob', 1]]).to eql([comp1, comp2])
    end
  end

  describe '#process_groups' do
    let(:single_group) { instance_double LSS::FileGroup }
    let(:complex_group) { instance_double LSS::FileGroup }
    before do
      expect(LSS::FileGroup).to receive(:new).with([1, 'bob'], [comp3]).and_return(single_group)
      expect(LSS::FileGroup).to receive(:new).with([1,'bob',1], [comp2]).and_return(complex_group)
      expect(complex_group).to receive(:add).with(comp1).and_return(true)
    end
    it 'assigns single variable groups to #resolved_groups' do
      result = described_class.new([comp1, comp2, comp3])
      result.process_groups
      expect(result.resolved_groups).to include(single_group)
    end
  end
end