require 'rspec'

RSpec.describe LSS::DirectoryReader do
  describe '#read_directory' do
    expected_filenames = %w(01.txt 02.txt 03.txt 04.txt 06.txt 7.txt
10.txt 10-11.txt 10-12.txt 11-11.txt 11-13.txt 11-13-11.txt 11-14-11.txt
11-15-11.txt 11-15-11)
    it 'returns the correct files' do
      expect(described_class.read_directory('./spec/test_files')).to include(*expected_filenames)
    end
  end
end