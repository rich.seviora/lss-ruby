require 'rspec'

module LSS
  RSpec.describe Manager do
    describe '.process' do
      let(:path) { '.' }
      let(:filenames) { ['123', '456'] }
      let(:file_obj1) { instance_double FileName }
      let(:file_obj2) { instance_double FileName }
      let(:grouper) { instance_double BlockGrouper, resolved_groups: [] }
      let(:renderer) {instance_double OutputRenderer}
      it 'operates correctly' do
        # Arrange
        expect(DirectoryReader).to receive(:read_directory).with(path).and_return(filenames)
        expect(FileName).to receive(:from_filename).with('123', :width).and_return(file_obj1)
        expect(FileName).to receive(:from_filename).with('456', :width).and_return(file_obj2)
        expect(BlockGrouper).to receive(:new).with([file_obj1, file_obj2]).and_return(grouper)
        expect(grouper).to receive(:process_groups)
        expect(OutputRenderer).to receive(:new).with(STDOUT, :width).and_return(renderer)
        expect(renderer).to receive(:render_groups).with([]).and_return(true)
        # Act
        described_class.process(path)
        # All assertions configured in Arrange block.
      end
      it 'raises ArgumentError for an invalid path' do
        expect{described_class.process('blahblahblah')}.to raise_error(ArgumentError)
      end
    end
  end
end
