require_relative '../src/lss'

if(ARGV[0] == '-v')
  LSS::Manager.process(ARGV[1], :value)
else
  LSS::Manager.process(ARGV[0])
end
